using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using ToDo.Models;
using Xunit;

namespace ToDo.IntegrationTests
{
    public class EndToEndTests : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> _factory;

        public EndToEndTests(WebApplicationFactory<Startup> factory)
        {
            _factory = factory.WithWebHostBuilder(config =>
            {
                config.ConfigureServices(services =>
                {
                    services.AddDbContext<ToDoContext>(Options =>
                    Options.UseInMemoryDatabase("InMemory"));
                });
            });
        } 
    

        [Theory]
        [InlineData("/api/ToDoItems")]
        [InlineData("/api/ToDoItems/1")]

        public async Task ApiRouteTest(string url)
        {
            var client = _factory.CreateClient();

            var response = await client.GetAsync(url);

            response.EnsureSuccessStatusCode();
            Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType.ToString());
        }
    }
}
