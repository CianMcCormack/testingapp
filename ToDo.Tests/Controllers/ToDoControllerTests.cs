﻿using System.Collections.Generic;
using System.Linq;
using Xunit;
using Moq;
using ToDo.Models.Interfaces;
using ToDo.Controllers;
using GenFu;
using ToDo.Models.Entities;

namespace ToDo.Tests.controllers
{
    public class ToDoControllerTests
    {

        //is there any sort of beforeEach that could be used to put the InitialTestData() into?
        [Fact]
        public void GetToDoItems_ReturnsAllToDoItems()
        {
            var service = new Mock<IToDoService>();
            var toDoItems = InitialTestData();
            service.Setup(x => x.GetAllItems()).Returns(toDoItems);

            var controller = new ToDoItemsController(service.Object);

            var results = controller.GetTodoItems();

            var count = results.Count();

            Assert.Equal(26, count);
        }

        [Fact]
        public void FindItemById_ExistingId_ReturnsCorrectItem()
        {
            var service = new Mock<IToDoService>();

            var Items = InitialTestData();
            var firstItem = Items.First();
            service.Setup(x => x.FindToDoItem(1)).Returns(firstItem);

            var controller = new ToDoItemsController(service.Object);

            var result = controller.GetToDoItem(1);
            var item = result.Value;

            Assert.Equal(1, item.Id);
        }

        [Fact]
        public void GetItemById_NonExistantId_ReturnsNotFound()
        {
            var service = new Mock<IToDoService>();

            var Items = InitialTestData();
            var firstItem = Items.First();
            service.Setup(x => x.FindToDoItem(1)).Returns(firstItem);


        }

        private IEnumerable<ToDoItem> InitialTestData()
        {

            var i = 1;
            var ToDoItem = A.ListOf<ToDoItem>(26);
            ToDoItem.ForEach(x => x.Id = i++);
            return ToDoItem.Select(_ => _);

        }
    }
}
