﻿using Xunit;
using ToDo.Models.Services;
using System.Linq;
using Moq;
using ToDo.Models;
using Microsoft.EntityFrameworkCore;
using GenFu;
using System.Collections.Generic;
using ToDo.Models.Entities;

namespace ToDo.Tests.Models.Services
{
    public class ToDoServiceTests
    {
        [Fact]
        public void ToDoServiceGetAllItemsTest()
        {
            var context = CreateToDoDbContext().Object;
            var service = new ToDoService(context);

            var results = service.GetAllItems();
            var count = results.Count();

            Assert.Equal(26, count);
        }

        private Mock<ToDoContext> CreateToDoDbContext()
        {
            var ToDoItems= InitialTestData().AsQueryable();

            var dbSet = new Mock<DbSet<ToDoItem>>();
            dbSet.As<IQueryable<ToDoItem>>().Setup(m => m.Provider).Returns(ToDoItems.Provider);
            dbSet.As<IQueryable<ToDoItem>>().Setup(m => m.Expression).Returns(ToDoItems.Expression);
            dbSet.As<IQueryable<ToDoItem>>().Setup(m => m.ElementType).Returns(ToDoItems.ElementType);
            dbSet.As<IQueryable<ToDoItem>>().Setup(m => m.GetEnumerator()).Returns(ToDoItems.GetEnumerator());

            var context = new Mock<ToDoContext>();
            context.Setup(c => c.ToDoItems).Returns(dbSet.Object);
            return context;
        }

        [Fact]
        public void FindItemByIdTest()
        {
            var context = CreateToDoDbContext().Object;
            var service = new ToDoService(context);

            var item = service.FindToDoItem(1);

            Assert.Equal(1, item.Id);
        }

        private IEnumerable<ToDoItem> InitialTestData()
        {
            var i = 1;
            var ToDoItem = A.ListOf<ToDoItem>(26);
            ToDoItem.ForEach(x => x.Id = i++);
            return ToDoItem.Select(_ => _);

        }
    }
}
