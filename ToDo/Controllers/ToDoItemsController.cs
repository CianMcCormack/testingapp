﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ToDo.Models.DTOs;
using ToDo.Models.Interfaces;
using ToDo.Models.Entities;

namespace ToDo.Controllers
{
    [Route("api/ToDoItems")]
    [ApiController]
    public class ToDoItemsController : ControllerBase
    {
        //using the interface to the DB context rather than a hardcoded context class instance allows us to test the controller by passing in mock objects easily
        private readonly IToDoService _ToDoService;

        public ToDoItemsController(IToDoService ToDoService)
        {
            _ToDoService = ToDoService;
        }

        // GET: api/ToDo/ToDoItems for public use, hiding the Secret on the item list entity
        // Here the controller performs a simple read of DB items, no writes or logic, so it's fine to just all be completed at this level
        [HttpGet]
        public IEnumerable<ToDoItemPublicViewDTO> GetTodoItems()
        {
            return _ToDoService.GetAllItems()
                .Select(x => ItemToDTO(x));
        }

        // GET: api/ToDo/ToDoItems/5
        // Here the controller performs a simple read of DB items, no writes or logic, so it's fine to just all be completed at this level
        [HttpGet("{id}")]
        public ActionResult<ToDoItemPublicViewDTO> GetToDoItem(int id)
        {
            var toDoItem = _ToDoService.FindToDoItem(id);

            if (toDoItem == null)
            {
                return NotFound("no Item exists with this ID");
            }

            return ItemToDTO(toDoItem);
        }

        // PUT: api/ToDoItems/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        /*// more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateToDoItem(long id, ToDoItem toDoItem)
        {
            if (id != toDoItem.Id)
            {
                return BadRequest();
            }

            _ToDoService.UpdateToDoItem(toDoItem);

            try
            {
                await _ToDoService.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ToDoItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }*/

        // POST: api/ToDoItems
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        /*[HttpPost]
        public async Task<ActionResult<ToDoItemPublicViewDTO>> CreateToDoItem(ToDoItemPublicViewDTO ToDoItemPublicViewDTO)
        {
            var todoItem = new ToDoItem
            {
                IsComplete = ToDoItemPublicViewDTO.IsComplete,
                Name = ToDoItemPublicViewDTO.Name
            };

            _ToDoService.GetAllItems().Add(todoItem);
            await _ToDoService.SaveChangesAsync();

            return CreatedAtAction(
                nameof(GetToDoItem), 
                new { id = todoItem.Id },
                ItemToDTO(todoItem));
        }

        // DELETE: api/ToDoItems/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ToDoItem>> DeleteToDoItem(long id)
        {
            var toDoItem = await _ToDoService.TodoItems.FindAsync(id);
            if (toDoItem == null)
            {
                return NotFound();
            }

            _ToDoService.TodoItems.Remove(toDoItem);
            await _ToDoService.SaveChangesAsync();

            return NoContent();
        }*/

        private bool ToDoItemExists(long id)
        {
            return _ToDoService.GetAllItems().Any(e => e.Id == id);
        }

        private static ToDoItemPublicViewDTO ItemToDTO(ToDoItem todoItem) =>
            new ToDoItemPublicViewDTO
            {
                Id = todoItem.Id,
                Name = todoItem.Name,
                IsComplete = todoItem.IsComplete
            };
    }
}
