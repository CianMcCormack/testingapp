﻿namespace ToDo.Models.DTOs
{
    public class ToDoItemPublicViewDTO
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool IsComplete { get; set; }
    
    }   
}
