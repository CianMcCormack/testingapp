﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDo.Models.Interfaces;
using ToDo.Models.Entities;

namespace ToDo.Models.Services
{
    public class ToDoService :IToDoService
    {

        private readonly ToDoContext _dbContext;
        public ToDoService(ToDoContext dbContext)
        {
            _dbContext = dbContext;
            _dbContext.Database?.EnsureCreated();
        }

        public async Task SaveChangesAsync()
        {
            var toDoItem = new ToDoItem();
            _dbContext.ToDoItems.Add(toDoItem);
            await _dbContext.SaveChangesAsync();
        }

        public IEnumerable<ToDoItem> GetAllItems()
        {
            return _dbContext.ToDoItems
                .OrderBy(x => x.Id)
                .ToList();
        }

        public ToDoItem FindToDoItem(int Id)
        {
            return _dbContext.ToDoItems
                .FirstOrDefault(x => x.Id == Id);
        }
    }
}
