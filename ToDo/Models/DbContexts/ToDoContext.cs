﻿using Microsoft.EntityFrameworkCore;
using GenFu;
using ToDo.Models.Entities;

namespace ToDo.Models
{
    public class ToDoContext : DbContext
    {
        public ToDoContext(DbContextOptions<ToDoContext> options)
            : base(options)
        {
        }

        public ToDoContext() { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // seeding
            var i = 1;
            var personsToSeed = A.ListOf<ToDoItem>(26);
            personsToSeed.ForEach(x => x.Id = i++);
            modelBuilder.Entity<ToDoItem>().HasData(personsToSeed);
        }


        public virtual DbSet<ToDoItem> ToDoItems { get; set; }

        
    }
}
