﻿using System;

namespace ToDo.Models.Entities
{
    public class ToDoItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool IsComplete { get; set; }
        public DateTime CompletionDate { get; set; }        
        public string Secret { get; set; }
    }
}
