﻿using System.Threading.Tasks;
using System.Collections.Generic;
using ToDo.Models.Entities;

namespace ToDo.Models.Interfaces
{
    public interface IToDoService
    {
        public Task SaveChangesAsync();
        public IEnumerable<ToDoItem> GetAllItems();
        public ToDoItem FindToDoItem(int Id);
    }
}
